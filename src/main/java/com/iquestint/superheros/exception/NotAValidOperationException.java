package com.iquestint.superheros.exception;

/**
 * <p>This is a simple custom exception thrown when an operation is not supported.</p>
 *
 * @author paul.stoia
 * @version 1.0
 */
public class NotAValidOperationException extends Exception {

    /**
     * Constructor of the custom exception.
     *
     * @param errorMessage the exception message to be displayed.
     * @since 1.0
     */
    public NotAValidOperationException(String errorMessage) {
        super(errorMessage);
    }
}
