package com.iquestint.superheros.model;

import com.iquestint.superheros.exception.NotAValidOperationException;

/**
 * <p>Monster game character type.</p>
 * <p>Contains only the attributes specific to the base type <i>{@link GameCharacter}</i>.</p>
 *
 * @author paul.stoia
 * @version 1.0
 * @see GameCharacter
 */
public class Monster extends GameCharacter {

    /**
     * <p>Build a game character of type monster.</p>
     * <p>Note that the character type is set automatically based on {@link RankType}.</p>
     *
     * @param name          the monster specific name.
     * @param description   the monster specific description.
     * @param attackSkills  the monster specific attack skills.
     * @param defenseSkills the monster specific defense skills.
     * @see RankType
     * @since 1.0
     */
    public Monster(String name, String description, int attackSkills, int defenseSkills) {
        super(name, description, attackSkills, defenseSkills);
    }

    /**
     * <p>This method is not supported yet by {@code {@link Monster}} game character type.</p>
     *
     * @param amount the experience amount value.
     * @throws NotAValidOperationException every time this method is called
     */
    @Override
    public void increaseExperience(int amount) throws NotAValidOperationException {
        throw new NotAValidOperationException("Monster do not have experience, so it can not be increased!");
    }

    /**
     * <p>This method is not supported yet by {@code {@link Monster}} game character type.</p>
     *
     * @param shield the item.
     * @throws NotAValidOperationException every time this method is called
     */
    @Override
    public void pickUpShield(Shield shield) throws NotAValidOperationException {
        throw new NotAValidOperationException("Monster do not have shield!");
    }

    /**
     * Get the total defence protection value.
     *
     * @return the total defense skills value divided by 5.
     */
    @Override
    public int getDefenseProtection() {
        return this.defenseSkills / 5;
    }

    /**
     * Get the total attack damage value.
     *
     * @param weapon the item that produces damage.
     * @return the weapon's damage {@code + attackSkills / 10.0}
     */
    @Override
    public double getAttackDamage(Weapon weapon) {
        return weapon.getDamage() + this.attackSkills / 10.0;
    }

    /**
     * Get the total healing amount value.
     *
     * @return always 1 for monster
     */
    @Override
    public int getHealingAmount() {
        return 1;
    }
}
