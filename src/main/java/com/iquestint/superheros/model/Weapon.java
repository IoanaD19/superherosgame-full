package com.iquestint.superheros.model;

/**
 * <p>Weapon game item type.</p>
 * <p>Besides the attributes specific to the base type <i>{@link GameItem}</i>, a weapon has a damage that it produces,
 * a {@code turnsToReload} which indicates the <b>static</b> numbers of turns needed to reload the weapon and a
 * {@code turnsTillActive} which indicates the <b>dynamic</b> numbers of turns that the weapon is still inactive.</p>
 *
 * @author paul.stoia
 * @version 1.0
 * @see GameItem
 */
public class Weapon extends GameItem {

    private int damage;
    private final int turnsToReload;
    private int turnsTillActive;

    /**
     * Build a game item of type weapon.
     * <p>Note that the {@code turnsTillActive} is set automatically to zero which means that the weapon is ready to be
     * used.</p>
     *
     * @param name          the weapon specific name.
     * @param description   the weapon specific description.
     * @param damage        the weapon specific damage.
     * @param turnsToReload the weapon specific turnsToReload.
     */
    public Weapon(String name, String description, int damage, int turnsToReload) {
        super(name, description);
        this.damage = damage;
        this.turnsToReload = turnsToReload;
        this.turnsTillActive = 0;
    }

    /**
     * Get the total damage value.
     *
     * @return the damage that this weapon produces.
     */
    public int getDamage() {
        return damage;
    }

    /**
     * Check if the weapon is active. An weapon is active if it does not require any turn to completely reload it.
     *
     * @return true if {@code turnsTillActive} is zero, false otherwise.
     */
    public boolean isActive() {
        return turnsTillActive == 0;
    }

    /**
     * Consume the weapon power one time. Note that after this method is called, the weapon will be not be active any
     * more unless {@code turnsToReload} number of turns is passed.
     *
     * @throws IllegalArgumentException if an illegal attempt to use the weapon during its loading process is done.
     */
    public void useWeapon() {
        if (!isActive()) {
            throw new IllegalArgumentException("Can't use an weapon that's not loaded.");
        }
        turnsTillActive = turnsToReload;
    }

    /**
     * Prepare for a new turn by decrementing the turns needed for the weapon to be completely ready to be used.
     */
    public void newTurn() {
        if (turnsTillActive > 0) {
            turnsTillActive--;
        }
    }
}
