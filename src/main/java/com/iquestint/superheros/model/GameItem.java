package com.iquestint.superheros.model;

/**
 * <p>Game item base type.</p>
 * <p>Contains only the attributes specific to the base item.</p>
 *
 * @author paul.stoia
 * @version 1.0
 */
public abstract class GameItem {

    private String name;
    private String description;

    /**
     * <p>Build a game character base item.</p>
     *
     * @param name        the game item specific name.
     * @param description the game item specific description.
     */
    GameItem(String name, String description) {
        this.name = name;
        this.description = description;
    }

    /**
     * Get the specific item name.
     *
     * @return the name that this item holds.
     */
    public String getName() {
        return name;
    }

    /**
     * Get the specific item description.
     *
     * @return the description that this item holds.
     */
    public String getDescription() {
        return description;
    }
}
