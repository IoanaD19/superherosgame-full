package com.iquestint.superheros.model;

/**
 * <p>Specific rank types to identify a {@link GameCharacter} player type.</p>
 * <p>MELEE - the character can fight closer to the opponent which means that defense skills are higher and the attack
 * ones not so much.</p>
 * <p>RANGE - the character can fight not so closer to the opponent because of the high attack skills.</p>
 * <p>BETWEEN - the character is between {@code MELEE} and {@code RANGE}.</p>
 *
 * <p>The hierarchical ascending view of this ranks is: {@code MELEE}, {@code BETWEEN}, {@code RANGE}. It is the best
 * case to hold a {@code RANGE} rank because you can attack from distance in order to increase attacks skills, also
 * experience and current health (depending on the case if health depends on attack skills or experience).</p>
 *
 * @author paul.stoia
 * @version 1.0
 * @see GameCharacter
 */
public enum RankType {
    MELEE, RANGE, BETWEEN
}
