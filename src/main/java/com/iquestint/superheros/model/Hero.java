package com.iquestint.superheros.model;

import com.iquestint.superheros.exception.NotAValidOperationException;

import java.util.LinkedList;
import java.util.List;

/**
 * <p>Hero game character type.</p>
 * <p>Besides the attributes specific to the base type <i>{@link GameCharacter}</i>, a hero has an experience and one
 * or more shields.</p>
 *
 * @author paul.stoia
 * @version 1.0
 * @see GameCharacter
 */
public class Hero extends GameCharacter {

    private int experience = 0;
    private List<Shield> shields = new LinkedList<>();

    /**
     * Build a game character of type hero.
     * <p>Note that the character type is set automatically based on {@link RankType}.</p>
     *
     * @param name          the hero specific name.
     * @param description   the hero specific description.
     * @param attackSkills  the hero specific attack skills.
     * @param defenseSkills the hero specific defense skills.
     * @see RankType
     * @since 1.0
     */
    public Hero(String name, String description, int attackSkills, int defenseSkills) {
        super(name, description, attackSkills, defenseSkills);
    }

    /**
     * Add an amount value to the hero experience.
     *
     * @param amount the experience amount to be added.
     * @throws NotAValidOperationException if the amount is negative.
     */
    @Override
    public void increaseExperience(int amount) throws NotAValidOperationException {
        if (amount < 0) {
            throw new NotAValidOperationException("The amount to increase experience can not be negative!");
        }
        this.experience += amount;
    }

    /**
     * Get the total defence protection value.
     *
     * @return all shields protection amount {@code + (defenseSkills + experience) / 10}
     */
    @Override
    public int getDefenseProtection() {
        int shieldProtection = 0;
        for (Shield shield : this.shields) {
            shieldProtection += shield.getProtection();
        }
        return shieldProtection + (this.defenseSkills + this.experience) / 10;
    }

    /**
     * Get the total attack damage value.
     *
     * @param weapon the item that produces damage.
     * @return the weapon's damage {@code + (attackSkills + experience) / 10.0}
     */
    @Override
    public double getAttackDamage(Weapon weapon) {
        return weapon.getDamage() + (this.attackSkills + this.experience) / 10.0;
    }

    /**
     * Get the total healing amount value.
     *
     * @return 1 if experience is zero, otherwise {@code 1 + experience / 10}
     */
    @Override
    public int getHealingAmount() {
        if (this.experience != 0) {
            return 1 + experience / 10;
        }
        return 1;
    }

    /**
     * Add this specific shield to hero's shields collection.
     *
     * @param shield the item to be added.
     * @throws NotAValidOperationException in case of the shield was not successfully added.
     */
    @Override
    public void pickUpShield(Shield shield) throws NotAValidOperationException {
        boolean pickedUpStatus = this.shields.add(shield);
        if (!pickedUpStatus) {
            throw new NotAValidOperationException("Picking up a shield for hero was not successfully finished!");
        }
    }
}
