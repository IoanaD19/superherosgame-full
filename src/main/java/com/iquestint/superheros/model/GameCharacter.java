package com.iquestint.superheros.model;

import com.iquestint.superheros.exception.NotAValidOperationException;

import java.util.LinkedList;
import java.util.List;

/**
 * <p>Game character base rankType.</p>
 * <p>Contains only the attributes specific to the base rankType.</p>
 *
 * @author paul.stoia
 * @version 1.0
 * @see RankType
 */
public abstract class GameCharacter {

    private String name;
    private String description;
    private RankType rankType;
    private List<Weapon> weapons = new LinkedList<>();

    int attackSkills;
    int defenseSkills;
    private double currentHealth;

    private static final Weapon DEFAULT_WEAPON = new Weapon("Fist", "Used when no other weapon available", 1, 0);

    /**
     * <p>Build a game character base rankType.</p>
     * <p>Note that the current health is set automatically to 100 and also a default weapon
     * {@code {@link GameCharacter#DEFAULT_WEAPON}} is provided.</p>
     *
     * @param name          the game character specific name.
     * @param description   the game character specific description.
     * @param attackSkills  the game character specific attack skills.
     * @param defenseSkills the game character specific defense skills.
     * @see RankType
     * @since 1.0
     */
    GameCharacter(String name, String description, int attackSkills, int defenseSkills) {
        this.name = name;
        this.description = description;
        this.attackSkills = attackSkills;
        this.defenseSkills = defenseSkills;
        this.currentHealth = 100;
        this.weapons.add(DEFAULT_WEAPON);
        updateRankType();
    }

    private void updateRankType() {
        if (defenseSkills >= 80 && attackSkills < 40) {
            rankType = RankType.MELEE;
        } else if (attackSkills >= 80) {
            rankType = RankType.RANGE;
        } else {
            rankType = RankType.BETWEEN;
        }
    }

    /**
     * Add an amount value to experience.
     *
     * @param amount the experience amount to be added.
     * @throws NotAValidOperationException if the operation is not supported for several reasons (e.g. the amount is
     *                                     negative or the character does not hold any experience).
     */
    public abstract void increaseExperience(int amount) throws NotAValidOperationException;

    /**
     * Add this specific shield to hero's shields collection.
     *
     * @param shield the item to be added.
     * @throws NotAValidOperationException if the operation is not supported for several reasons (e.g. the shield was
     *                                     not successfully added or the character does not hold any shields).
     */
    public abstract void pickUpShield(Shield shield) throws NotAValidOperationException;

    /**
     * Get total defence protection value.
     *
     * @return the total defence protection value.
     */
    public abstract int getDefenseProtection();

    /**
     * Get total attack damage value.
     *
     * @param weapon the item that produces damage.
     * @return the total attack damage value.
     */
    public abstract double getAttackDamage(Weapon weapon);

    /**
     * Get total healing amount value.
     *
     * @return the total healing amount value.
     */
    public abstract int getHealingAmount();

    /**
     * Check if the game character is still alive.
     *
     * @return true if the character is alive, false otherwise.
     */
    public boolean isAlive() {
        return currentHealth >= 0;
    }

    /**
     * Prepare for a new round of the game by updating the current health and the states of all weapons.
     */
    public void newTurn() {
        this.currentHealth = this.currentHealth + getHealingAmount();
        if (currentHealth > 100) {
            this.currentHealth = 100;
        }
        for (Weapon weapon : this.weapons) {
            weapon.newTurn();
        }
    }

    /**
     * @return the rank type of the game character
     * @see RankType
     */
    public RankType getRankType() {
        return rankType;
    }

    /**
     * @return the description which defines the character
     */
    public String getDescription() {
        return description;
    }

    /**
     * Get the most powerful weapon that this character holds.
     *
     * @return the weapon which produces the biggest damage.
     */
    public Weapon getBestWeapon() {
        Weapon bestWeapon = null;
        int maxDamage = 0;
        for (Weapon weapon : this.weapons) {
            if (weapon.isActive() && (weapon.getDamage() > maxDamage)) {
                maxDamage = weapon.getDamage();
                bestWeapon = weapon;
            }
        }
        return bestWeapon;
    }

    /**
     * Get all specific weapons.
     *
     * @return the list contains all weapons that this character holds.
     */
    public List<Weapon> getWeapons() {
        return weapons;
    }

    /**
     * Get the specific character name.
     *
     * @return the name that this character holds.
     */
    public String getName() {
        return name;
    }

    /**
     * Add a weapon to the weapons list that this character holds.
     *
     * @param weapon the item to be added.
     */
    public void pickUpWeapon(Weapon weapon) {
        this.weapons.add(weapon);
    }

    /**
     * Get the specific character current health.
     *
     * @return the current health that this character holds.
     */
    public double getCurrentHealth() {
        return this.currentHealth;
    }

    /**
     * Set the specific character current health.
     *
     * @param currentHealth the new current health to be set.
     */
    public void setCurrentHealth(double currentHealth) {
        this.currentHealth = currentHealth;
    }
}
