package com.iquestint.superheros.model;

/**
 * <p>Shield game item type.</p>
 * <p>Besides the attributes specific to the base type <i>{@link GameItem}</i>, a shield has a protection.</p>
 *
 * @author paul.stoia
 * @version 1.0
 * @see GameItem
 */
public class Shield extends GameItem {

    private double protection;

    /**
     * Build a game item of type shield.
     *
     * @param name          the shield specific name.
     * @param description   the shield specific description.
     * @param protection    the shield specific protection.
     */
    public Shield(String name, String description, double protection) {
        super(name, description);
        this.protection = protection;
    }

    /**
     * Get the protection value.
     *
     * @return the protection that this item holds.
     */
    public double getProtection() {
        return protection;
    }
}
