package com.iquestint.superheros.game;

import com.iquestint.superheros.exception.NotAValidOperationException;
import com.iquestint.superheros.model.*;

import java.lang.invoke.MethodHandles;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <p>Contains a game sample to prove the utility of all pieces comes together in one or more fight between heroes and
 * monsters.</p>
 *
 * @author paul.stoia
 * @version 1.0
 */
public class GameDemo {

    private static final Logger LOG = Logger.getLogger(MethodHandles.lookup().lookupClass().getName());

    /**
     * The main entry point.
     *
     * @param args arguments from command line.
     */
    public static void main(String[] args) {
        GameCharacter hero = new Hero("Brave Ballador", "Paladin protecting the Urn of King Terenas", 15, 10);
        GameCharacter monster = new Monster("Ar-dular", "Black dragon living in Black Forrest", 25, 10);

        Weapon sword = new Weapon("Knight sword", "Double edge blade.", 4, 0);
        Weapon electricStorm = new Weapon("Electric storm", "High amperage discharge", 15, 3);

        Shield elfArmour = new Shield("Elf Armour", "Light Elf Armour", 2);

        try {
            hero.pickUpShield(elfArmour);
        } catch (NotAValidOperationException ex) {
            LOG.log(Level.SEVERE, "Picking up a shield failed!", ex);
        }
        hero.pickUpWeapon(electricStorm);

        monster.pickUpWeapon(sword);

        Game game = new Game(hero, monster);
        game.start();
        try {
            hero.increaseExperience(10);
        } catch (NotAValidOperationException ex) {
            LOG.log(Level.SEVERE, "Increasing experience failed!", ex);
        }
        game.engageCharactersInBattle();
        game.performNewTurn();
        game.engageCharactersInBattle();
        game.end();
    }

}
