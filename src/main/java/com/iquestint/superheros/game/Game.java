package com.iquestint.superheros.game;

import com.iquestint.superheros.model.GameCharacter;
import com.iquestint.superheros.model.Weapon;

import java.lang.invoke.MethodHandles;
import java.text.DecimalFormat;
import java.util.logging.Logger;

/**
 * <p>The game controller engaged a hero and a monster.</p>
 *
 * @author paul.stoia
 * @version 1.0
 */
public class Game {

    private GameCharacter hero;
    private GameCharacter monster;

    private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.##");
    private static final Logger LOG = Logger.getLogger(MethodHandles.lookup().lookupClass().getName());

    /**
     * Build up a game for a specific hero and a specific monster.
     *
     * @param hero    the specific hero engaged in battle.
     * @param monster the specific monster engaged in battle.
     */
    public Game(GameCharacter hero, GameCharacter monster) {
        this.hero = hero;
        this.monster = monster;
    }

    /**
     * Prepare game to start (just logging a message) and render each character by logging the statistics.
     */
    public void start() {
        renderStartGame();
        renderCharacter(hero);
        renderCharacter(monster);
    }

    private void renderStartGame() {
        LOG.info("Game starts");
    }

    private void renderCharacter(GameCharacter character) {
        StringBuilder renderMessageBuilder = new StringBuilder("\"").append(character.getName());
        if (character.isAlive()) {
            renderMessageBuilder.append("\": health = ").append(character.getCurrentHealth());
        } else {
            renderMessageBuilder.append("\" is dead.");
        }
        String renderMessage = renderMessageBuilder.toString();
        LOG.info(renderMessage);
    }

    /**
     * <p>Starts battle between characters.</p>
     * <p>Choose the most active powerful weapons for each character (hero and monster), use them, update health for
     * each one depending on the other strike strength and finish fight by rendering the statistics for each character.</p>
     */
    public void engageCharactersInBattle() {
        LOG.info("Fight begins.");
        Weapon heroWeapon = hero.getBestWeapon();
        double heroStrikeStrength = hero.getAttackDamage(heroWeapon);
        renderShot(hero, heroWeapon, heroStrikeStrength, monster);

        Weapon monsterWeapon = monster.getBestWeapon();
        double monsterStrikeStrength = monster.getAttackDamage(monsterWeapon);
        renderShot(monster, monsterWeapon, monsterStrikeStrength, hero);

        heroWeapon.useWeapon();
        monsterWeapon.useWeapon();

        updateHealth(hero, monsterStrikeStrength);
        updateHealth(monster, heroStrikeStrength);

        renderCharacter(hero);
        renderCharacter(monster);

        LOG.info("Fight ends.");
    }

    private void renderShot(GameCharacter attacker, Weapon weapon, double strikeStrength, GameCharacter defender) {
        String shotMessage = "\"" + attacker.getName() + "\"" +
                " hit \"" + defender.getName() +
                "\" with " + weapon.getName() +
                ". Strength of hit: " + DECIMAL_FORMAT.format(strikeStrength) +
                " damage.";
        LOG.info(shotMessage);
    }

    private void updateHealth(GameCharacter character, double strengthOfStrike) {
        double totalProtection = (double) character.getDefenseProtection();
        if (strengthOfStrike > totalProtection) {
            double newHealthLevel = character.getCurrentHealth() + totalProtection - strengthOfStrike;
            character.setCurrentHealth(newHealthLevel);
        }
    }

    /**
     * Perform a new turn for each of the 2 characters and render them by logging the statistics.
     */
    public void performNewTurn() {
        hero.newTurn();
        monster.newTurn();

        renderCharacter(hero);
        renderCharacter(monster);
    }

    /**
     * Finish the game by logging with a message.
     */
    public void end() {
        LOG.info("Game ends");
    }
}
