package com.iquestint.superheros;

import com.iquestint.superheros.exception.NotAValidOperationException;
import com.iquestint.superheros.model.*;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.lang.invoke.MethodHandles;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.hamcrest.Matchers.is;

public class GameCharacterTest {

    private GameCharacter hero;

    private static final Logger LOG = Logger.getLogger(MethodHandles.lookup().lookupClass().getName());

    @Before
    public void setUp() {
        hero = new Hero("Hero1", "Hero1 description", 30, 10);
    }

    @Test
    public void shouldHaveMaxHealthWhenCreated() {
        Assert.assertThat(hero.getCurrentHealth(), is(100.0));
    }

    @Test
    public void shouldHaveDefaultWeaponWhenCreated() {
        Weapon heroWeapon = hero.getBestWeapon();

        Assert.assertThat(heroWeapon, CoreMatchers.notNullValue());
        Assert.assertThat(heroWeapon.getDamage(), is(1));
    }

    @Test
    public void shouldHaveInitialProtectionAccordingToDefensiveSkills() {
        Assert.assertThat(hero.getDefenseProtection(), is(1));
    }

    @Test
    public void protectionShouldIncreaseWhenExperienceIncreases() {
        try {
            hero.increaseExperience(40);
        } catch (NotAValidOperationException ex) {
            LOG.log(Level.SEVERE, "Increasing experience failed!", ex);
            Assert.assertTrue("Unsupported operation for this type of character!", false);
        }
        Assert.assertThat(hero.getDefenseProtection(), is(5));
    }

    @Test
    public void protectionShouldIncreaseWhenAddingAShield() {
        try {
            hero.pickUpShield(new Shield("shield1", "shield1 description", 20));
        } catch (NotAValidOperationException ex) {
            LOG.log(Level.SEVERE, "Picking up a shield failed!", ex);
            Assert.assertTrue("Unsupported operation for this type of character!", false);
        }
        Assert.assertThat(hero.getDefenseProtection(), is(21));
    }

    @Test
    public void shouldHoldBetweenRank() {
        Assert.assertThat(hero.getRankType(), is(RankType.BETWEEN));
    }

    @Test
    public void shouldHoldDescription() {
        Assert.assertThat(hero.getDescription(), is("Hero1 description"));
    }

    @Test
    public void shouldHoldDefaultWeapon() {
        Assert.assertThat(hero.getWeapons().size(), is(1));
        Assert.assertThat(hero.getWeapons().get(0).getDescription(), is("Used when no other weapon available"));
    }
}
