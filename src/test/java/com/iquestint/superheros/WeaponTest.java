package com.iquestint.superheros;

import com.iquestint.superheros.model.Weapon;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import org.junit.Assert;

public class WeaponTest {

    @Test
    public void shouldBeDisabledAfterOneUseIfUsedAndNeedsReloading() {
        Weapon weapon = new Weapon("dagger", "dagger description", 1, 2);
        weapon.useWeapon();
        Assert.assertThat(weapon.isActive(), is(false));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionIfUsedWhenDisabled() {
        Weapon weapon = new Weapon("dagger", "dagger description", 1, 2);
        weapon.useWeapon();
        Assert.assertThat(weapon.isActive(), is(false));
        weapon.useWeapon();
        Assert.assertThat(weapon.isActive(), is(true));
    }

    @Test
    public void shouldBeEnabledWhenBuilt() {
        Weapon weapon = new Weapon("dagger", "dagger description", 1, 2);
        Assert.assertThat(weapon.isActive(), is(true));
    }
}
