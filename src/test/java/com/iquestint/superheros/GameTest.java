package com.iquestint.superheros;

import com.iquestint.superheros.game.Game;
import com.iquestint.superheros.model.GameCharacter;
import com.iquestint.superheros.model.Hero;
import com.iquestint.superheros.model.Monster;
import com.iquestint.superheros.model.Weapon;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;

public class GameTest {

    private Game game;

    @Test
    public void shouldEngage2CharactersInBattle() {
        GameCharacter popeye = new Hero("Popeye", "Sailor", 15, 1);
        Weapon harpoon = new Weapon("harpoon", "For hunting whales.", 2, 1);
        popeye.pickUpWeapon(harpoon);
        GameCharacter bluto = new Monster("Bluto", "Big and bad", 8, 3);

        game = new Game(popeye, bluto);
        game.engageCharactersInBattle();

        Assert.assertThat(popeye.getCurrentHealth(), CoreMatchers.equalTo(98.2));
        Assert.assertThat(bluto.getCurrentHealth(), CoreMatchers.equalTo(96.5));
        Assert.assertThat(harpoon.isActive(), CoreMatchers.equalTo(false));
    }

    @Test
    public void shouldExecuteHealthUpdatesAndWeaponReloadWhenTurnIsOver() {
        GameCharacter popeye = new Hero("Popeye", "Sailor", 15, 1);
        GameCharacter bluto = new Monster("Bluto", "Big and bad", 8, 3);

        Weapon harpoon = new Weapon("harpoon", "For hunting whales.", 2, 1);
        popeye.pickUpWeapon(harpoon);

        game = new Game(popeye, bluto);
        game.engageCharactersInBattle();
        game.performNewTurn();

        Assert.assertThat(popeye.getCurrentHealth(), CoreMatchers.equalTo(99.2));
        Assert.assertThat(bluto.getCurrentHealth(), CoreMatchers.equalTo(97.5));
        Assert.assertThat(harpoon.isActive(), CoreMatchers.equalTo(true));
    }
}
